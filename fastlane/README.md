fastlane documentation
================
# Installation
```
sudo gem install fastlane
```
# Available Actions
## Mac
### mac test
```
fastlane mac test
```
Run All Tests
### mac custom
```
fastlane mac custom
```
Build a Custom (Unofficial) Release

Configure and build a single use release that doesn't support updates.
### mac alpha
```
fastlane mac alpha
```
Build and Prepare an Alpha for Upload

Configure and build a release for the Alpha channel.
### mac beta
```
fastlane mac beta
```
Build and Prepare an Beta for Upload

Configure and build a release for the Beta channel.
### mac update
```
fastlane mac update
```
Build and Prepare a Release to Group

Prepare the current commit as an update for release to the specified percentage of users. Rounds to the nearest of these: 5, 15, 25, 50, 75 or 100.

Required: run this lane with 'percentage' and an integer for amount

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [https://fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [GitHub](https://github.com/fastlane/fastlane/tree/master/fastlane).
