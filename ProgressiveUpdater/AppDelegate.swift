//
//  Created by Brian Ganninger on 11/23/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import Cocoa
import Sparkle

@NSApplicationMain

class AppDelegate: NSObject, NSApplicationDelegate
{
	@IBOutlet weak var updateGroupNumber: NSTextField!
	@IBOutlet weak var updateGroupPercentage: NSTextField!
	@IBOutlet weak var lastUpdatedDate: NSTextField!
	
	var updateHelper: PRGUpdateHelper?
	var sparkleUpdater: SUUpdater?
	
	// MARK: - Application Delegate -
	
	func applicationDidFinishLaunching(aNotification: NSNotification)
	{
		updateHelper = PRGUpdateHelper()
		sparkleUpdater = updateHelper!.newSparkleUpdater()
		
		// unnecessary in demo app
		sparkleUpdater!.sendsSystemProfile = false
		sparkleUpdater!.automaticallyChecksForUpdates = false
		
		regenerateGroup()
	}

	func applicationShouldTerminateAfterLastWindowClosed(sender: NSApplication) -> Bool
	{
		return true
	}
	
	// MARK: - UI Actions -
	
	@IBAction func reassignGroup(sender: AnyObject)
	{
		NSUserDefaults.standardUserDefaults().removeObjectForKey(PRGUpdateGroupDefaultsKey)
		regenerateGroup()
	}
	
	@IBAction func checkForUpdates(sender: AnyObject)
	{
		sparkleUpdater!.checkForUpdates(sender)
		updateUI()
	}
	
	// MARK: - Helpers -
	
	func regenerateGroup()
	{
		// NOTES: The update helper class should always attempt to reset when an app launches;
		// it will only change the group when the app's version (1.0 -> 1.0.1) is bumped.
		PRGUpdateHelper.resetUpdateGrouping()
		updateUI()
	}
	
	func updateUI()
	{
		let groupNum = updateHelper!.currentUpdateGroup()
		let groupTransformer = PRGGroupPercentageTransformer.init()
		let percentageValue = groupTransformer.transformedValue(groupNum as NSNumber) as! NSNumber
		
		updateGroupNumber.integerValue = groupNum
		updateGroupPercentage.stringValue = NSLocalizedString("GroupPercentage", comment: "").stringByReplacingOccurrencesOfString("%i", withString:String(percentageValue.integerValue))
		lastUpdatedDate.objectValue = sparkleUpdater!.lastUpdateCheckDate
	}
}
