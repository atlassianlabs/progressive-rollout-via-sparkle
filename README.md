Progressive Rollout via Sparkle
===============================

When using [Sparkle](https://sparkle-project.org) to provide in-app updates on macOS the default behavior is that an update is available for all users simultaneously whenever the `Appcast.xml` file is updated. 

Modern agile methodologies, such as those found in services, favor staging the rollout of updated functionality to an increasingly larger group of users. This strategy provides a safety valve for issues that weren't caught during testing or that crop up unexpectedly, like a signing issue.

Here you'll find a thoroughly tested collection of classes in a sample project along with notes for additional changes you might want to make to your process. If you have any questions, concerns, or improvements please reach out per the notes below. 

Originally implemented and utilized in [SourceTree](https://www.sourcetreeapp.com). 

Dependencies
============

These classes are meant to be used **in conjunction** with [Sparkle](https://sparkle-project.org); they are not a replacement. [Fastlane](https://fastlane.tools/) is an *optional* way to facilitate deployment but both options will be discussed.

[Carthage](https://github.com/Carthage/Carthage) is the preferred option for this project. To get set up in your checkout run `carthage update --platform Mac` which will checkout and build managed dependencies. After that you'll be able to build and run *ProgressiveUpdater*.

Documentation
=============

Please refer to the following documents for details:

* [CODE OF CONDUCT.md](CODE_OF_CONDUCT.md) - Community Code of Conduct; **play nice!**
* [CONTRIBUTING.md](CONTRIBUTING.md) - Pull Request Guidelines
* [ARCHITECTURE.md](ARCHITECTURE.md) - Details on client, scripting, and assorted other aspects required for deployment
* *Headers* - All classes include HeaderDoc compatible comments for easy access

Tests
=====

The simplest way to run tests is to use `scan` from the [Fastlane](https://fastlane.tools/) suite. All unit tests and their expectations will be exercised. You can also run and debug them in Xcode when necessary. 

Contributors
============

Pull requests, issues, and comments are welcome provided they conform to our [Code of Conduct](CODE_OF_CONDUCT.md). See the existing issues for things to start contributing. When starting a pull request please be sure to follow our [Contributing Guidelines](CONTRIBUTING.md) to speed up review.

For bigger proposed changes, make sure you start a discussion first by creating an issue and explaining the intended change.

[Atlassian](https://www.atlassian.com) requires contributors to sign a Contributor License Agreement, known as a CLA. This serves as a record stating that the contributor is entitled to contribute the code/documentation/translation to the project and is willing to have it used in distributions and derivative works (or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

License
=======

Copyright (c) 2016 [Atlassian](https://www.atlassian.com) and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
