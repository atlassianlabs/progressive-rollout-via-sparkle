ARCHITECTURE: Progressive Rollout via Sparkle
==============================================

An overview of what's provided in this project and how to use them in your own app.

### Goals

* no dependency changes
* client self-assigns update group
* ability to release in stages *(group 0 at 5%, group 1 at 15%, etc.)*
* no impact on existing alpha/beta distribution
* minimal file duplication on server and no web service
* automatable process

### Core Classes (client)

Adding support for progressive rollout to your app is straightforward; the essential logic and API are encapsulated in the following classes you include in your project:

* `PRGUpdateHelper` - the primary class, configured as an `SUUpdater` delegate, that randomizes with six predefined groups *(5%, 15%, 25%, 50%, 75%, 100%)* and handles feed assignment
* `NSString+PRGHashing` - a utility category on NSString to generate a unique numeric string to use during randomization; required by `PRGUpdateHelper`, not used directly
* `PRGGroupPercentageTransformer` *(optional)* - a value tranformer for converting a group to its human readable percentage (i.e., 2 returns 25%)
* `PRGTestProgressiveUpdates` *(optional)* - a set of unit tests offering 100% code coverage to add to your suite

### Server (update contents)

There's not a large amount of change compared to what's currently present when using [Sparkle](https://sparkle-project.org) which is ideal; essentially we're just adding copies of relevant content with group names included. A release's binary is pushed like normal with access to a particular one gated by which feeds it's present in.

appcast (update) XML feeds - Appcast.xml (legacy, no further updates), AppcastAlpha.xml and AppcastBeta.xml stay the same, adds AppcastGroup0.xml through AppcastGroup5.xml

* ***per version release notes HTML*** *(suggested)* - it makes things simpler when having multiple feeds to reference a distinct file for each release; servers are made to handle lots of them in a directory just fine 😀 *ReleaseNotesAlpha.html* and *ReleaseNotesBeta.html* are typically sufficient and are automatically detected for group updates.
* ***pre-progressive rollout [Sparkle](https://sparkle-project.org) Appcast*** - no change to the current Appcast file; this allows existing users to upgrade to the version with new updating logic (and then they can update again to get to latest.)
* ***Alpha/Beta Appcasts*** *(suggested)* - each of these channels should also have an Appcast file *(AppcastAlpha.xml, AppcastBeta.xml)* corresponding to the release notes discussed previously
* ***progressive rollout Appcasts*** - a single Appcast file is maintained in the repository; during deployment, as discussed below, the script will upload as many times as necessary with the appropriate group name

### Fastlane (deployment) *optional*

How you choose to automate these deployment steps is ultimately a per-project decision. An easy to use cross-platform option for automating these channels and their updates is [Fastlane](https://fastlane.tools). Simplified versions of what's used for [SourceTree](https://www.sourcetreeapp.com/) are included in this project to jumpstart other developers.
