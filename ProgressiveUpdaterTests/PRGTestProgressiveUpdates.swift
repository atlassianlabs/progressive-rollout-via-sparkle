//
//  Created by Brian Ganninger on 9/30/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import XCTest
import Sparkle

class STTestProgressiveUpdates: XCTestCase
{
	// MARK: - Tests -
	
	func testUpdaterBasics()
	{
		let updateHelper = PRGUpdateHelper()
		let updater = updateHelper.newSparkleUpdater()
		let defaults = NSUserDefaults.standardUserDefaults()
		let currentGroup = defaults.valueForKey(PRGUpdateGroupDefaultsKey)
		let currentAppVersion = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString")
		
		// test convenience method for setup
		XCTAssertNotNil(updater)
		XCTAssertNotNil(updateHelper)
		XCTAssertEqual(updater.delegate as? PRGUpdateHelper, updateHelper)
		
		// test convenience accessor for assigned group
		let expectedGroup = 2
		defaults.setValue(expectedGroup, forKey:PRGUpdateGroupDefaultsKey)
		XCTAssertEqual(expectedGroup, PRGUpdateHelper.currentUpdateGroup())
		
		// test the 'auto reset' group assignment
		defaults.removeObjectForKey(PRGUpdateGroupDefaultsKey)
		defaults.removeObjectForKey(PRGUpdateVersionDefaultsKey)
		defaults.removeObjectForKey(PRGUpdateExemptionDefaultsKey)
		XCTAssertNil(defaults.objectForKey(PRGUpdateGroupDefaultsKey))
		XCTAssertNil(defaults.objectForKey(PRGUpdateVersionDefaultsKey))
		XCTAssertNil(defaults.objectForKey(PRGUpdateExemptionDefaultsKey))
		PRGUpdateHelper.resetUpdateGrouping()
		XCTAssertNotNil(defaults.objectForKey(PRGUpdateGroupDefaultsKey))
		XCTAssertEqual(currentAppVersion as? String, defaults.stringForKey(PRGUpdateVersionDefaultsKey))
		XCTAssertLessThanOrEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), 5)
		XCTAssertGreaterThanOrEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), 0)
		XCTAssertEqual(defaults.integerForKey(PRGUpdateExemptionDefaultsKey), 2)
		
		defaults.removeObjectForKey(PRGUpdateGroupDefaultsKey)
		XCTAssertNil(defaults.objectForKey(PRGUpdateGroupDefaultsKey))
		XCTAssertNotNil(defaults.objectForKey(PRGUpdateVersionDefaultsKey))
		XCTAssertEqual(defaults.integerForKey(PRGUpdateExemptionDefaultsKey), 2)
		defaults.setInteger(404, forKey:PRGUpdateExemptionDefaultsKey)
		PRGUpdateHelper.resetUpdateGrouping()
		XCTAssertEqual(currentAppVersion as? String, defaults.stringForKey(PRGUpdateVersionDefaultsKey))
		XCTAssertNotNil(defaults.objectForKey(PRGUpdateGroupDefaultsKey))
		XCTAssertLessThanOrEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), 5)
		XCTAssertGreaterThanOrEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), 0)
		XCTAssertEqual(defaults.integerForKey(PRGUpdateExemptionDefaultsKey), 2)
		
		defaults.removeObjectForKey(PRGUpdateVersionDefaultsKey)
		XCTAssertNotNil(defaults.objectForKey(PRGUpdateGroupDefaultsKey))
		XCTAssertNil(defaults.objectForKey(PRGUpdateVersionDefaultsKey))
		XCTAssertEqual(defaults.integerForKey(PRGUpdateExemptionDefaultsKey), 2)
		PRGUpdateHelper.resetUpdateGrouping()
		XCTAssertEqual(currentAppVersion as? String, defaults.stringForKey(PRGUpdateVersionDefaultsKey))
		XCTAssertNotNil(defaults.objectForKey(PRGUpdateGroupDefaultsKey))
		XCTAssertLessThanOrEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), 5)
		XCTAssertGreaterThanOrEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), 0)
		XCTAssertEqual(defaults.integerForKey(PRGUpdateExemptionDefaultsKey), 2)
		
		let assignedGroup = defaults.integerForKey(PRGUpdateGroupDefaultsKey)
		defaults.setValue(0.9, forKey:PRGUpdateVersionDefaultsKey)
		XCTAssertNotNil(defaults.objectForKey(PRGUpdateGroupDefaultsKey))
		XCTAssertNotNil(defaults.objectForKey(PRGUpdateVersionDefaultsKey))
		XCTAssertEqual(defaults.integerForKey(PRGUpdateExemptionDefaultsKey), 2)
		PRGUpdateHelper.resetUpdateGrouping()
		XCTAssertEqual(currentAppVersion as? String, defaults.stringForKey(PRGUpdateVersionDefaultsKey))
		XCTAssertNotNil(defaults.objectForKey(PRGUpdateGroupDefaultsKey))
		XCTAssertLessThanOrEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), 5)
		XCTAssertGreaterThanOrEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), 0)
		XCTAssertEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), assignedGroup)
		XCTAssertEqual(defaults.integerForKey(PRGUpdateExemptionDefaultsKey), 1)
		defaults.setValue(0.9, forKey:PRGUpdateVersionDefaultsKey)
		PRGUpdateHelper.resetUpdateGrouping()
		XCTAssertEqual(currentAppVersion as? String, defaults.stringForKey(PRGUpdateVersionDefaultsKey))
		XCTAssertNotNil(defaults.objectForKey(PRGUpdateGroupDefaultsKey))
		XCTAssertLessThanOrEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), 5)
		XCTAssertGreaterThanOrEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), 0)
		XCTAssertEqual(defaults.integerForKey(PRGUpdateExemptionDefaultsKey), 2)
		
		defaults.setValue(0.9, forKey:PRGUpdateVersionDefaultsKey)
		defaults.removeObjectForKey(PRGUpdateExemptionDefaultsKey)
		PRGUpdateHelper.resetUpdateGrouping()
		XCTAssertEqual(currentAppVersion as? String, defaults.stringForKey(PRGUpdateVersionDefaultsKey))
		XCTAssertNotNil(defaults.objectForKey(PRGUpdateGroupDefaultsKey))
		XCTAssertLessThanOrEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), 5)
		XCTAssertGreaterThanOrEqual(defaults.integerForKey(PRGUpdateGroupDefaultsKey), 0)
		XCTAssertEqual(defaults.integerForKey(PRGUpdateExemptionDefaultsKey), 2)
		
		defaults.setValue(expectedGroup, forKey:PRGUpdateGroupDefaultsKey)
		defaults.setValue(currentAppVersion, forKey:PRGUpdateVersionDefaultsKey)
		PRGUpdateHelper.resetUpdateGrouping()
		XCTAssertEqual(expectedGroup, defaults.integerForKey(PRGUpdateGroupDefaultsKey))
		XCTAssertEqual(currentAppVersion as? String, defaults.stringForKey(PRGUpdateVersionDefaultsKey))

		defaults.setValue(currentGroup, forKey:PRGUpdateGroupDefaultsKey)
	}
	
	func testSparkleDelegate()
	{
		let updateHelper = PRGUpdateHelper()
		let updater = updateHelper.newSparkleUpdater()
		
		// optional update suppression
		updateHelper.canUpdate = true
		XCTAssertTrue(updateHelper.updaterMayCheckForUpdates(updater))
		updateHelper.canUpdate = false
		XCTAssertFalse(updateHelper.updaterMayCheckForUpdates(updater))
		
		// feed URL override
		let defaults = NSUserDefaults.standardUserDefaults()
		let expectedGroup = 2
		let currentGroup = defaults.valueForKey(PRGUpdateGroupDefaultsKey)
		defaults.setValue(expectedGroup, forKey:PRGUpdateGroupDefaultsKey)
		XCTAssertEqual("https://www.sourcetreeapp.com/update/labs/DemoAppcastGroup2.xml",
		               updateHelper.feedURLStringForUpdater(updater))
		defaults.setValue(currentGroup, forKey:PRGUpdateGroupDefaultsKey)
	}
	
	func testUpdateGroupsAccuracy()
	{
		let uuidIterations: Double = 100000
		let uuidBase: Double = uuidIterations / 100
		let allowedVariance = 1.0
		
		var group0Total = 0.0
		var group1Total = 0.0
		var group2Total = 0.0
		var group3Total = 0.0
		var group4Total = 0.0
		var group5Total = 0.0
		
		var loopIndex: Double = 0
		while (loopIndex < uuidIterations)
		{
			let aGroup = PRGUpdateHelper.groupForUUID(NSUUID())
			
			switch aGroup
			{
				case 0:
					group0Total += 1
					break
				case 1:
					group1Total += 1
					break
				case 2:
					group2Total += 1
					break
				case 3:
					group3Total += 1
					break
				case 4:
					group4Total += 1
					break
				case 5:
					group5Total += 1
					break
				default:
					break
			}
			
			loopIndex += 1
		}
		
		XCTAssertEqualWithAccuracy(round((group0Total / uuidBase)), 5, accuracy:allowedVariance)
		XCTAssertEqualWithAccuracy(round((group1Total / uuidBase)), 10, accuracy:allowedVariance)
		XCTAssertEqualWithAccuracy(round((group2Total / uuidBase)), 10, accuracy:allowedVariance)
		XCTAssertEqualWithAccuracy((round(group3Total / uuidBase)), 25, accuracy:allowedVariance)
		XCTAssertEqualWithAccuracy((round(group4Total / uuidBase)), 25, accuracy:allowedVariance)
		XCTAssertEqualWithAccuracy((round(group5Total / uuidBase)), 25, accuracy:allowedVariance)
	}
	
	func testFeedURLTransformations()
	{
		let updateHelper = PRGUpdateHelper()
		let appcastSlug = "Appcast.xml"
		let defaultURL = "https://www.sourcetreeapp.com/update/labs/DemoAppcast.xml"
		
		// URL checks
		let group0 = defaultURL.stringByReplacingOccurrencesOfString(appcastSlug, withString:"AppcastGroup0.xml")
		XCTAssertEqual(group0, updateHelper.feedURLForGroup(0))
		let group1 = defaultURL.stringByReplacingOccurrencesOfString(appcastSlug, withString:"AppcastGroup1.xml")
		XCTAssertEqual(group1, updateHelper.feedURLForGroup(1))
		let group2 = defaultURL.stringByReplacingOccurrencesOfString(appcastSlug, withString:"AppcastGroup2.xml")
		XCTAssertEqual(group2, updateHelper.feedURLForGroup(2))
		let group3 = defaultURL.stringByReplacingOccurrencesOfString(appcastSlug, withString:"AppcastGroup3.xml")
		XCTAssertEqual(group3, updateHelper.feedURLForGroup(3))
		let group4 = defaultURL.stringByReplacingOccurrencesOfString(appcastSlug, withString:"AppcastGroup4.xml")
		XCTAssertEqual(group4, updateHelper.feedURLForGroup(4))
		let group5 = defaultURL.stringByReplacingOccurrencesOfString(appcastSlug, withString:"AppcastGroup5.xml")
		XCTAssertEqual(group5, updateHelper.feedURLForGroup(5))
		
		// bounds checks
		XCTAssertEqual(defaultURL, updateHelper.feedURLForGroup(-1))
		XCTAssertEqual(defaultURL, updateHelper.feedURLForGroup(54986))
	}
	
	func testValueTransformer()
	{
		let groupTransformer = PRGGroupPercentageTransformer.init()
		
		// actual groupings
		XCTAssertEqual(groupTransformer.transformedValue(0) as? NSNumber, 5 as NSNumber)
		XCTAssertEqual(groupTransformer.transformedValue(1) as? NSNumber, 15 as NSNumber)
		XCTAssertEqual(groupTransformer.transformedValue(2) as? NSNumber, 25 as NSNumber)
		XCTAssertEqual(groupTransformer.transformedValue(3) as? NSNumber, 50 as NSNumber)
		XCTAssertEqual(groupTransformer.transformedValue(4) as? NSNumber, 75 as NSNumber)
		XCTAssertEqual(groupTransformer.transformedValue(5) as? NSNumber, 100 as NSNumber)
		
		// bounds checks
		XCTAssertEqual(groupTransformer.transformedValue(-1) as? NSNumber, -1 as NSNumber)
		XCTAssertEqual(groupTransformer.transformedValue(54986) as? NSNumber, -1 as NSNumber)
	}
}
