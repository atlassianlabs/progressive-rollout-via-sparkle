# Introduction

This is a set of guidelines centered around contributing to our community and the project. Please keep these in mind when preparing or reviewing a pull request. They aren't meant to be annoying, just a baseline.

# Project

The long-term health of any project is quite important; these are a few guidelines to follow to keep ours in top shape.

## Tests

There aren't a huge number of unit tests but we need to ensure they stay in proper form. Ideally we're ensuring the most code coverage possible with each change made.

Test failures are not allowed on master and **must** be fixed and reviewed prior to merging.

## Warnings

Note the number of general warnings prior to your change with a clean build. Repeat with your changes. These should be the same or less with your changes. The goal is to maintain zero build warnings. 

If you cannot avoid a warning be sure to note this and the reason for it in your PR as well as with a comment in the code if possible.

## Static Analyzer

Note the number of analyzer results prior to your change with a clean build. Repeat with your changes. These should be the same or less with your changes.

If you cannot avoid triggering a new result be sure to note this and the reason for it in your PR as well as with a comment in the code if possible.

# Code

Source code comprises the majority of what we read and write on a daily basis. These guidelines are meant to ensure a smooth experience today, tomorrow, and much further down the road.

## Tabs vs Spaces

*Can't we all just get along?*

Let `\t` be your guide. Please ensure your favorite editor is configured to respect this; Xcode can be set to use tabs (Preferences -> Editing -> Indentation) and allows you to choose how that character is displayed.

## Prefixing

*Avoiding conflicts the old-fashioned way.*

Classes, constants, enums, flags, and a host of other types all benefit from the convention of prefixing. The default prefix in our project is **PRG**, regardless of the particular target.

## ARC

*Leave it to the compiler… mostly.*

Automated Retain Counting has been the de facto option for a while now. That doesn't mean you're free of memory management gotchas but it does eliminate a chunk of common ones. Be careful with blocks.

If you need an exception this can be done on a per-file basis; please note the reason(s) why in a comment near the top of the file, at the call site necessitating the exception, and in the PR's description.

## Nullability

*To be or not to be… that is the question.*

Swift's type safety led to a shiny new bit of annotations for Objective C node - [nullability for parameters and returns](https://developer.apple.com/swift/blog/?id=25). As the name implies, these inform users and compilers about whether or not a method or function either returns nils or can handle them as a particular parameter. APIs (i.e., anything in a header) should always include the appropriate keywords.
